package com.example.photoalbum.Asymmetric.Asymmetric;

public interface PoolObjectFactory<T> {
  T createObject();
}
