package com.example.photoalbum.Asymmetric.Asymmetric;

public class SampleData {

    private int a;

    public SampleData(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }
}
